# Arch Linux - Instalación (Intel - Nvidia) modo UEFI-GPT

## Preparación

- Descargar iso desde [Arch Linux](https://www.archlinux.org/download/)
- Crear usb bootable

Desde linux:
```
sudo dd bs=4M if=/ruta/a/archlinux.iso of=/dev/sdX status=progress && sync
```
Desde Windows, usar [Rufus](https://rufus.akeo.ie/?locale)
- Verificar la integridad del archivo iso con **GPG** **MD5** y **SHA1**
- Verificar modo UEFI/EFI en la bios

> *mas info [wiki](https://wiki.archlinux.org/index.php/Category:Getting_and_installing_Arch_(Espa%C3%B1ol))*

## Preinstalación

### Distribución de teclado

Para buscar distribuciones de teclado 
```
localectl list-keymaps
```
Para buscar una distribución, reemplazar `BUSQUEDA` con el codigo de lenguaje
```
localectl list-keymaps | grep -i BUSQUEDA
```
Distribución latam 
```
loadkeys la-latin1
```

### Verificar modo de arranque

Si el modo UEFI esta habilitado en la placa base UEFI, Archiso arrancará a través de `systemd-boot`. Para comprobar:
```
ls /sys/firmware/efi/efivars
efivar -l
```
Si el directorio no existe, *revisar la configuración de la placa*

### Conexión a internet

Comprobar con
```
ping -c 5 archlinux.org
```

### Actualizar reloj del sistema

```
timedatectl set-ntp true
timedatectl set-timezone America/Bogota
timedatectl status
```

### Particionado del disco

Identificar dispositivos con `lsblk` o `fdisk -l`

Particiones necesarias:
- Partición del sistema `EFI`
- Partición para el directorio root `(/)`

### Borrar tablas de particiones 

:warning: Este proceso borrara por completo el dispositivo seleccionado, *volver a identificar los dispositivos* :warning:

***Crear GPT partition table***
```
gdisk /dev/sdX
```
> sd**X**, **X** representa la letra del dispositivo ej. *a*, *b*, *c*...
- Presionar [[X]] para ir al menu extendido
- Presionar [[Z]] para hacer *zap*
- Presionar [[Y]] para confirmar
- Presionar [[Y]] para borrar MBR
>![79DqLIc.png](https://i.imgur.com/79DqLIc.png)

#### Crear particiones

> Tabla de particiones recomendada
> ![Xfev4La.png](https://i.imgur.com/Xfev4La.png)

- Usar `gdisk /dev/sdX` luego presionar [[N]] para añadir una nueva partición
- Partition number (1-128, default N): presionar [[Intro]]
- First sector (default) luego presionar [[Intro]]
- Last sector: `+-"Cantidad asignar"{KMGTP}`.
> Cantidad recomendada
>    - 500M para Boot
>    - mas de 30G para (/)
>    - *de acuerdo al usuario* para (/home)
>    - *de acuerdo al usuario* para [Swap]
- Hex code or GUID:
    - Sistema EFI `ef00`, para BIOS `ef02`
    - Linux x86-64 root (/) `8304`
    - Linux swap [Swap] `8200`
    - Linux home (/home) `8302`
> Ejemplo para crear partición `efi`:
>
> ![WZPuCjk.png](https://i.imgur.com/WZPuCjk.png)
>
> En caso de tener 100G para /home y Swap, y se quiere dar 2G para swap, usar -2G para home y quedara los 2G para swap:
>
> ![CcNV0Mn.png](https://i.imgur.com/CcNV0Mn.png)

Una vez creada las particiones verificar con [[P]], luego presionar [[W]] y luego [[Y]] para confirmar

#### Formateo de las particiones

- Para EFI se usa `fat32` 
```
mkfs.fat -F32 /dev/sdXn
```
- Para `(/)` y `/home` se usa `ext4`
```
mkfs.ext4 /dev/sdXn
```
- Para Swap se usa 
```
mkswap /dev/sdXn
swapon /dev/sdXn
```

> Ejemplo con tabla de partición recomendada
> ![yCcuRAr.png](https://i.imgur.com/yCcuRAr.png)

#### Montaje de los sistemas de archivos

Montar el sistema de archivos de la partición raíz (/) en `/mnt`
```
mount /dev/sda2 /mnt
```

Crear puntos de montaje para las particiones restante
```
mkdir /mnt/boot
mkdir /mnt/home
mount /dev/sda1 /mnt/boot
mount /dev/sda3 /mnt/home
```
> En caso de tener partición EFI, raíz (/) y Swap omitir el paso de `mkdir /mnt/home` y `mount /dev/sda3 /mnt/home`

## Instalación

### Instalar sistema base

```
pacstrap -i /mnt base base-devel
```

## Configuración del sistema

### Fstab

Generar archivo `fstab`
```
genfstab -U -p /mnt >> /mnt/etc/fstab
```
- Verificar el archivo `/mnt/etc/fstab` y modificar en caso de errores

### Chroot

```
arch-chroot /mnt
```

### Zona horaria

```
ln -s /usr/share/zoneinfo/America/Bogota > /etc/localtime
```
- Ejecutar `hwclock --systohc` para generar `/etc/adjtime`

### Idioma del sistema

```
nano /etc/locale.gen
```
- Descomentar el locale deseado, para Colombia buscar `es_CO.UTF-8 UTF-8`
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

- Generar locale
```
locale-gen
echo LANG=es_CO.UTF-8 > /etc/locale.conf
export LC_ALL="en_US.UTF-8"
```
- Distribucion de teclado
```
echo KEYMAP=la-latin1 > /etc/vconsole.conf
```

### Configuración de red

Crear archivo hostname "nombre de la maquina":
```
hostnamectl set-hostname NOMBREMAQUINA
```

### Initramfs

```
mkinitcpio -p linux
```

### Contraseña de superusuario (root)

Establezca contraseña del superusuario:
```
passwd
```

### Crear usuario

```
useradd -m -g users -G wheel,storage,power -s /bin/bash NOMBREUSUARIO -c "NOMBRE COMPLETO"
passwd NOMBREUSUARIO
```

Editar `/etc/sudoers`

```
nano /etc/sudoers
```
- Descomentar `%wheel ALL=(ALL) ALL`

### Gestor de arranque (Systemd-boot)

```
pacman -S intel-ucode
bootctl --path=/boot install
```

- Editar `/boot/loader/entries/arch.conf`
```
nano /boot/loader/entries/arch.conf
```
- Añadir
```
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

Añadir `PARTUUID` de la partición raíz (/)
> :warning: Verificar la particion raíz (/) antes de continuar con `lsblk` o `fdisk -l` y reemplazar `/dev/sdXn` con la partición raíz creada, ej. `dev/sda2` :warning:
```
echo "options root=PARTUUID=$(blkid -s PARTUUID -o value /dev/sdXn) rw" >> /boot/loader/entries/arch.conf
```
Instalar NetworkManager
```
pacman -S networkmanager
systemctl enable NetworkManager.service
```

## Reinicio

```
exit
umount -R /mnt
reboot
```

## Posinstalación

### Hook para systemd boot

Actualizar automaticamente systemd-boot cuando exista una actualización de `systemd`. Crear una carpeta `hooks` y crear un archivo de texto `systemd-boot.hook`
```
sudo mkdir /etc/pacman.d/hooks
sudo nano /etc/pacman.d/hooks/systemd-boot.hook
```
Agregar en `systemd-boot.hook`
```
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

### Loader configuration (Optional)

#### :warning: No se podra editar en caso de que falle

[systemd-boot - ArchWiki](https://wiki.archlinux.org/index.php/systemd-boot#Loader_configuration) *whether to enable the kernel parameters editor or not. yes (default) is enabled, no is disabled; since the user can add init=/bin/bash to bypass root password and gain root access, it is strongly recommended to set this option to no*
```
sudo nano /boot/loader/loader.conf
```
Agregar 
```
editor 0
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

### Agregar tiempo de inicio para systemd-boot (Optional)

Revisar [systemd-boot - ArchWiki](https://wiki.archlinux.org/index.php/systemd-boot#Loader_configuration), si no se específica tiempo, el sistema inicia automaticamente
```
sudo nano /boot/loader/loader.conf
```
Agregar
```
timeout 5
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir


> ![dFQKDko.png](https://i.imgur.com/dFQKDko.png)

### Habilitar Multilib y AUR

Ejecutar `sudo nano /etc/pacman.conf`

Descomentar `[multilib]` y agregar # AUR
```
[multilib]
Include = /etc/pacman.d/mirrorlist

# AUR
[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/$arch
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

- Luego actualizar `sudo pacman -Syyu`

### Instalar [YAY](https://github.com/Jguer/yay), Yet another Yogurt - An AUR Helper written in Go

```
sudo pacman -S git
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
yay --editmenu --nodiffmenu --save
```

Editar `/etc/pacman.conf` y descomentar las siguientes lineas
```
#Misc options
UseSyslog
Color
TotalDownload
CheckSpace
#VerbosePkgLists
```

### Instalar NVIDIA Drivers

```
sudo pacman -S linux-headers
sudo pacman -S xorg-{server,apps,xinit,twm,xclock} 
sudo pacman -S nvidia nvidia-{utils,libgl,settings} lib32-nvidia-{utils,libgl}
```
> Elegir `nvidia-utils`

Extra
```
sudo pacman -S vulkan-icd-loader lib32-vulkan-icd-loader
sudo pacman -S xterm mesa mesa-demos curl gnupg wget zip
```

#### Agregar Hook

```
sudo nano /etc/pacman.d/hooks/nvidia.hook
```
Agregar
```
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia

[Action]
Description=Update Nvidia module in initcpio
Depends=mkinitcpio
When=PostTransaction
Exec=/usr/bin/mkinitcpio -p linux
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

```
systemctl enable nvidia-persistenced.service
```

#### Habilitar

Agregar los modulos en el archivo `/etc/mkinitcpio.conf`:
- nvidia
- nvidia_modeset
- nvidia_uvm
- nvidia_drm

Editar `/etc/mkinitcpio.conf`
```
sudo nano /etc/mkinitcpio.conf
```
> ![9TwUeDn.png](https://i.imgur.com/9TwUeDn.png)

> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

Agregar `nvidia-drm.modeset=1` en los parametros del kernel

Editar `/boot/loader/entries/arch.conf`
```
sudo nano /boot/loader/entries/arch.conf
```
Agregar `nvidia-drm.modeset=1` al final de `options root=... rw`

> ![zrjIZqQ.png](https://i.imgur.com/zrjIZqQ.png)

> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

Ejecutar
```
sudo mkinitcpio -p linux
```

### Instalación de fuentes
```
sudo pacman -S ttf-{hack,anonymous-pro,dejavu,freefont,liberation,roboto}
sudo pacman -S ttf-{bitstream-vera,croscore,droid,ubuntu-font-family}
yay -S ttf-{ms-fonts,monaco,emojione-color,twemoji-color} noto-fonts-emoji
```

---

## Instalar entorno grafico

### Instalar [KDE - Plasma 5](https://gitlab.com/Gabeqb/arch-linux-instalacion/blob/master/Entornos%20de%20Escritorio/KDE%20-%20Plasma%205.md)
### Instalar [Xfce 4](https://gitlab.com/Gabeqb/arch-linux-instalacion/blob/master/Entornos%20de%20Escritorio/Xfce4.md)

## Extra

### ZSH y Ohmyzsh

```
sudo pacman -S zsh
chsh -s /bin/zsh
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

### Screentearing con nvidia

Generar `xorg.conf`
```
sudo nvidia-xconfig
```

editar `xorg.conf`
```
sudo nano /etc/X11/xorg.conf
```
Agregar en `Section "Screen"`:
- `Option "metamodes" "nvidia-auto-select +0+0 {ForceCompositionPipeline=On}"`
- `Option "AllowIndirectGLXProtocol" "off"`
- `Option "TripleBuffer" "on"`

>Muestra
```
Section "Device"
        Identifier "Nvidia Card"
        Driver     "nvidia"
        VendorName "NVIDIA Corporation"
        BoardName  "GeForce GTX 1050 Ti"
EndSection

Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
    Option         "metamodes" "nvidia-auto-select +0+0 {ForceCompositionPipeline=On}"
    Option         "AllowIndirectGLXProtocol" "off"
    Option         "TripleBuffer" "on"
EndSection
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

Para multi monitor revisar [Wiki](https://wiki.archlinux.org/index.php/NVIDIA/Troubleshooting#Multi-monitor)

Abrir nvidia settings
> Configuración recomendada
>
> ![](https://i.imgur.com/TGM4YXc.png)

#### Para KDE - Plasma 5 revisar [Ajuste KDE - Nvidia](https://gitlab.com/Gabeqb/arch-linux-instalacion/blob/master/Entornos%20de%20Escritorio/KDE%20-%20Plasma%205.md#ajuste-kde-nvidia)

#### Screentearing en Firefox

Revisar si `OpenGL OMTC` este activado, ir a `about:support` y en la sección "Graphics" buscar por "Compositing". Si reporta "Basic", OpenGL OMTC esta desactivado; si reporta "OpenGL" esta habilitado.

Para activar OpenGL OMTC, ir a `about:config` y habilitar `layers.acceleration.force-enabled`, luego reiniciar firefox.

### Wine y Dependencias

```
sudo pacman -S wine{-staging,-mono,_gecko,tricks} 
```

```
sudo pacman -S giflib lib{png,pulse,ldap,jpeg-turbo,xcomposite,xinerama,xslt,va} gnutls mpg123 openal v4l-utils alsa-{lib,plugins} ncurses opencl-icd-loader gtk3 gst-plugins-base-libs cups samba dosbox lib32-{giflib,lib{png,pulse,ldap,jpeg-turbo,xcomposite,xinerama,xslt,va},gnutls,mpg123,openal,v4l-utils,alsa-{lib,plugins},ncurses,opencl-icd-loader,gtk3,gst-plugins-base-libs}
```
```
yay -S ncurses5-compat-libs lib32-ncurses5-compat-libs
```

### Steam y Lutris

```
sudo pacman -S steam steam-native-runtime lutris
yay -S steam-fonts steam-fonts
```

### VirtualBox

```
sudo pacman -S virtualbox virtualbox-gest-iso
```
> Elegir `virtualbox-host-modules-arch`
```
sudo pacman -S net-tools
sudo modprobe vboxdrv
sudo gpasswd -a $USER vboxusers
```