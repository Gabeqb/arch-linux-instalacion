# Aplicaciones

## Internet

### Clientes BitTorrent
 - **QBittorrent** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Transmission** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Clientes *Cloud*
 - **Dropbox** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **MegaSync** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Clientes de e-mail
 - **Thunderbird** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Claws Mail**

### Clientes RSS
 - **Akregator**.
 - **Liferea**.

### Navegadores web
 - **Firefox** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Google Chrome** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Brave**

### *Social*
 - **Telegram** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Discord** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Whatsie** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Messenger for Desktop** - **Plataformas:** *GNU/Linux, Windows, MacOS*. [Pagina](https://messengerfordesktop.com/).

## Multimedia

### Manipulación de gráficos e imágenes
 - **GIMP** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Krita** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Modelado 3D.
 - **Blender** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Captura de pantalla
 - **OBS** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Simplescreenrecorder**.

### Reproducción de audio/video
 - **Spotify** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **VLC** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Editores de audio/video
 - **Audacity** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Kdenlive** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

## Utilidades

### Gestores de contraseñas.
 - **KeePass** - **Plataformas:** *GNU/Linux, Windows*.
 - **KeePassXC** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Bara de tareas
 - **Plank** [Wiki](https://wiki.archlinux.*org/index*.php/Plank)
 - **Docky**
 - **Latte Dock**

## Documentos

### Lectores de documentos
 - **Calibre** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Evince** lector de PDF, Comics (CBR, CBZ, CB7, CBT) - **Plataformas:** *GNU/Linux, Windows*.
 - **FBReader** lector epub, fb2, chm, etc.. - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **YacReader** lector de Comics - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Suites de ofimática
 - **LibreOffice** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **WPS Office** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### *Notas*
 - **Boostnote** - **Plataformas:** *GNU/Linux, Windows, MacOS*. [Pagina](https://boostnote.io/)
 - **Turtl App** - **Plataformas:** *GNU/Linux, Windows, MacOS*. [Pagina](https://turtlapp.com/)
 - **NixNote2** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Editores de texto
 - **Gedit** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Mousepad** - **Plataformas:** *GNU/Linux*.
 - **Kate**

## Otros

### Lanzadores de aplicaciones
 - **Albert** - **Plataformas:** *GNU/Linux*. [GitHub](https://github.com/albertlauncher/albert)

---

# Later hehexd

### Gaming.
 - **Steam** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Lutris** - **Plataformas:** *GNU/Linux*.

### IDE.
 - **Visual Studio Code** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Peliculas, Series, etc...
 - **PopcornTime** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **Stremio** - **Plataformas:** *GNU/Linux, Windows, MacOS*.

### Otros.
 - **GParted**.
 - **Redshift**.
 - **Electrum BTC Wallet**.
 - **MuseScore**.
 - **Bleachbit**.
 - **Sweeper**.
 - **Caffeine-ng**.
 - **MenuLibre**.
 - **PSensor**.
 - **CMapTools**.
 - **Cryptomator** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - **VeraCrypt** - **Plataformas:** *GNU/Linux, Windows, MacOS*.
 - Onionshare
 - whormhole
 - Trello Desktop