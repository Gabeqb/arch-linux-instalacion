# XFCE4 instalación

```
echo "exec startxfce4" > /home/$USER/.xinitrc
```

```
sudo pacman -S xfce4{,-goodies} arc-gtk-theme papirus-icon-theme hunspell{,-es_any,-en_US}
sudo pacman -S gnome-disk-utility ufw gufw htop transmission-gtk evince
sudo systemctl start ufw.service
sudo systemctl enable ufw.service
```

### Audio 
```
sudo pacman -S alsa-utils pulseaudio{,-asla} libcanberra-pulse vlc
```
### Instalar display manager para XFCE
```
sudo pacman -S lightdm{,-gtk-greeter{,-settings}}
sudo systemctl enable lightdm
```

Editar `/etc/lightdm/lightdm.conf`
```
sudo nano /etc/lightdm/lightdm.conf
```
Buscar `greeter-session=`
Agregar
```
greeter-session=lightdm-gtk-greeter
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir

```
reboot
```
## CONFIGURAR DISTRIBUCION DE TECLADO
> ![MsHvy5k.png](https://i.imgur.com/MsHvy5k.png)