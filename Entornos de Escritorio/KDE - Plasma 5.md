# KDE - Plasma 5 instalación

```
echo "exec startkde" > /home/$USER/.xinitrc
```

## Escritorio base
```
sudo pacman -S plasma-{desktop,workspace{,-wallpapers},pa,nm} kwin kwrited
sudo pacman -S breeze breeze-gtk kde{-gtk-config,libs4support,plasma-addons}
sudo pacman -S papirus-icon-theme
```
Algunos complementos
```
sudo pacman -S plasma5-applets-redshift-control hunspell{,-es_any,-en_US}
sudo pacman -S kdialog khelpcenter user-manager
sudo pacman -S qtcurve-{utils,kde,gtk2} powerdevil kinfocenter
sudo pacman -S appmenu-{gtk-module,qt4} libdbusmenu-{glib,gtk2,gtk3}
```

## Programas
```
sudo pacman -S ark konsole kate dolphin dolphin-plugins gwenview spectacle
sudo pacman -S gnome-disk-utility ufw gufw kscreen htop ffmpegthumbs
sudo pacman -S kcalc kdegraphics-thumbnailers transmission-qt evince
sudo systemctl start ufw.service
sudo systemctl enable ufw.service
```

### Audio
```
sudo pacman -S alsa-utils pulseaudio{,-asla} libcanberra-{pulse,gstreamer}
sudo pacman -S gst-{libav,plugins-{bad,good,ugly}} vlc pavucontrol

```

## Instalar display manager para KDE Plasma
```
sudo pacman -S sddm sddm-kcm
sudo systemctl enable sddm
reboot
```

## Ajustes KDE Plasma

Abrir Preferencias del sistemas, buscar Dispositivos de entrada, en Teclado ir a distribuciones y elegir la configuración de teclado, quitar la de `us` y aplicar
>![ogT25Pd.png](https://i.imgur.com/ogT25Pd.png)

Abrir terminal Konsole, editar `/etc/sddm.conf`
```
sudo nano /etc/sddm.conf
```
Agregar
```
[Theme]
# Current theme name
Current=breeze

# Cursor theme used in the greeter
CursorTheme=breeze_cursors
```
y reiniciar.

## Screentearing con nvidia

Generar `xorg.conf`
```
sudo nvidia-xconfig
```

editar `xorg.conf`
```
sudo nano /etc/X11/xorg.conf
```
Agregar en `Section "Screen"`:
- `Option "metamodes" "nvidia-auto-select +0+0 {ForceCompositionPipeline=On}"`
- `Option "AllowIndirectGLXProtocol" "off"`
- `Option "TripleBuffer" "on"`
>Muestra

```
Section "Device"
        Identifier "Nvidia Card"
        Driver     "nvidia"
        VendorName "NVIDIA Corporation"
        BoardName  "GeForce GTX 1050 Ti"
EndSection

Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
    Option         "metamodes" "nvidia-auto-select +0+0 {ForceCompositionPipeline=On}"
    Option         "AllowIndirectGLXProtocol" "off"
    Option         "TripleBuffer" "on"
EndSection
```
Para multi monitor revisar [Wiki](https://wiki.archlinux.org/index.php/NVIDIA/Troubleshooting#Multi-monitor)

Abrir nvidia settings
> Configuración recomendada
>
> ![](https://i.imgur.com/TGM4YXc.png)

### Ajuste KDE - Nvidia

Abrir Preferencias del sistemas y buscar Compositor
> Ajuste recomendado
![](https://i.imgur.com/J1x9Voy.png)

Ejecutar
```
sudo mkdir /etc/profile.d
sudo nano /etc/profile.d/kwin.sh
```
Agregar
```
#!/bin/sh
export KWIN_TRIPLE_BUFFER=1
```
> [[CTRL]] + [[O]] para guardar, [[Intro]] para confirmar y [[CTRL]] + [[X]] para salir


![](https://i.imgur.com/I4fbcCK.png)

![](https://i.imgur.com/w5QcJ1l.png)